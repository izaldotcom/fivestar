<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_master extends CI_Model {

		function getUser(){
				$query = $this->db->get('user');
				return $query;
			}

		function tambahUser($array) {
				$query=$this->db->insert('user',$array);
				return $query;
			}

		function editUser($id,$array) {
				$this->db->where('id_user', $id);
				$query = $this->db->update('user',$array); 	
				return $query;
			}

		function deleteUser($id) {
				$query =  $this->db->query('DELETE FROM user WHERE id_user = '.$id);
				return $query;
			}

		function kodeterapis()   {
		  $this->db->select('id_terapis as kode', FALSE);
		  $this->db->order_by('id_terapis','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('m_terapis');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "TR-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
		}

		function getGradeTerapis(){
				$query = $this->db->get('m_grade_terapis');
				return $query;
			}	

		function tambahGradeTerapis($array) {
				$query=$this->db->insert('m_grade_terapis',$array);
				return $query;
			}

		function editGradeTerapis($id_grade,$array) {
				$this->db->where('id_grade', $id_grade);
				$query = $this->db->update('m_grade_terapis',$array); 	
				return $query;
			}


		function deleteGradeTerapis($id) {
				$query =  $this->db->query('DELETE FROM m_grade_terapis WHERE id_grade = '.$id);
				return $query;
			}
  
		
		function getTerapis(){
			$query = $this->db->query('SELECT a.*, b.* from m_terapis a
					JOIN m_grade_terapis b
					ON a.id_grade=b.id_grade
					ORDER BY a.id_terapis ASC');
				return $query;
			}

		function tambahTerapis($array) {
				$query=$this->db->insert('m_terapis',$array);
				return $query;
			}

		function editTerapis($id_terapis,$array) {
				$this->db->where('id_terapis', $id_terapis);
				$query = $this->db->update('m_terapis',$array); 	
				return $query;
			}


		function deleteTerapis($id) {
				$query =  $this->db->query('DELETE FROM m_terapis WHERE id_terapis = '.$id);
				return $query;
			}

		function kodecustomer()   {
		  $this->db->select('id_customer as kode', FALSE);
		  $this->db->order_by('id_customer','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('m_customer');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "CST-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
		}	

		function getCustomer(){
				$query = $this->db->get('m_customer');
				return $query;
		}	

		function tambahCustomer($array) {
				$query=$this->db->insert('m_customer',$array);
				return $query;
			}

		function editCustomer($id_customer,$array) {
				$this->db->where('id_customer', $id_terapis);
				$query = $this->db->update('m_customer',$array); 	
				return $query;
			}


		function deleteCustomer($id) {
				$query =  $this->db->query('DELETE FROM m_customer WHERE id_customer = '.$id);
				return $query;
			}

		function kodesu()   {
		  $this->db->select('id_su as kode', FALSE);
		  $this->db->order_by('id_su','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('m_servis_utama');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "SU-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
		}	

		function getSU(){
				$query = $this->db->get('m_servis_utama');
				return $query;
			}	

		function tambahSU($array) {
				$query=$this->db->insert('m_servis_utama',$array);
				return $query;
			}

		function editSsU($id_su,$array) {
				$this->db->where('id_su', $id_su);
				$query = $this->db->update('m_servis_utama',$array); 	
				return $query;
			}

		function editSU($where,$table){		
			return $this->db->get_where($table,$where);
		}

		function deleteSU($id) {
				$query =  $this->db->query('DELETE FROM m_servis_utama WHERE id_su = '.$id);
				return $query;
			}

		function kodest()   {
		  $this->db->select('id_st as kode', FALSE);
		  $this->db->order_by('id_st','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('m_servis_tambahan');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "ST-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
		}	

		function getST(){
				$query = $this->db->get('m_servis_tambahan');
				return $query;
			}	

		function tambahST($array) {
				$query=$this->db->insert('m_servis_tambahan',$array);
				return $query;
			}

		function editST($id_st,$array) {
				$this->db->where('id_st', $id_st);
				$query = $this->db->update('m_servis_tambahan',$array); 	
				return $query;
			}


		function deleteST($id) {
				$query =  $this->db->query('DELETE FROM m_servis_tambahan WHERE id_st = '.$id);
				return $query;
			}

		function getRev(){
				$query = $this->db->query('SELECT a.*, b.kode_terapis, b.nama_terapis, c.kode_su, c.nama_su, c. rate_su, c.diskon_su, 
d.kode_st, d.nama_st, d.rate_st, d.diskon_st
FROM tr_revenue a 
INNER JOIN m_terapis b ON a.id_terapis=b.id_terapis
INNER JOIN m_servis_utama c ON a.id_su=c.id_su
INNER JOIN m_servis_tambahan d ON a.id_st=d.id_st
ORDER BY a.tgl_rev ASC');
				return $query;
			}	

		function tambahRev($array) {
				$query=$this->db->insert('tr_revenue',$array);
				return $query;
			}

		function editRev($id_rev,$array) {
				$this->db->where('id_rev', $id_rev);
				$query = $this->db->update('tr_revenue',$array); 	
				return $query;
			}


		function deleteRev($id) {
				$query =  $this->db->query('DELETE FROM tr_revenue WHERE id_rev = '.$id);
				return $query;
			}

		function get_grade_id($id_grade){
		$hsl=$this->db->query("SELECT * FROM m_grade_terapis WHERE id_grade='$id_grade'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'grade_type' => $data->grade_type,
					'deskripsi' => $data->deskripsi,
					);
			}
		}
		return $hasil;
		}	


		function get_terapis_id($id_terapis){
		$hsl=$this->db->query("SELECT * FROM m_terapis WHERE id_terapis='$id_terapis'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'kode_terapis' => $data->kode_terapis,
					'nama_terapis' => $data->nama_terapis,
					'rate_terapis' => $data->rate_terapis,
					);
			}
		}
		return $hasil;
		}

		function get_su_id($id_su){
		$hsl=$this->db->query("SELECT * FROM m_servis_utama WHERE id_su='$id_su'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'kode_su' => $data->kode_su,
					'nama_su' => $data->nama_su,
					'rate_su' => $data->rate_su,
					'diskon_su' => $data->diskon_su,
					);
			}
		}
		return $hasil;
		}	

		function get_st_id($id_st){
		$hsl=$this->db->query("SELECT * FROM m_servis_tambahan WHERE id_st='$id_st'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'kode_st' => $data->kode_st,
					'nama_st' => $data->nama_st,
					'rate_st' => $data->rate_st,
					'diskon_st' => $data->diskon_st,
					);
			}
		}
		return $hasil;
		}			

		

}

?>