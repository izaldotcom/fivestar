<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class revenue extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
		if($this->session->userdata('status') != "login"){
		   redirect(base_url("admin/login"));
		  }
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);

		//$this->load->view('welcome_message');
		$data = '';

		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['terapis'] = $this->m_master->getTerapis()->result();
		$this->data['su'] = $this->m_master->getSU()->result();
		$this->data['st'] = $this->m_master->getST()->result();
		$this->data['revenue'] = $this->m_master->getRev()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('master/revenue', $this->data);
	}

	public function get_terapis(){
		$id_terapis=$this->input->post('id_terapis');
		$data=$this->m_master->get_terapis_id($id_terapis);
		echo json_encode($data);
	}

	public function get_su(){
		$id_su=$this->input->post('id_su');
		$data=$this->m_master->get_su_id($id_su);
		echo json_encode($data);
	}

	public function get_st(){
		$id_st=$this->input->post('id_st');
		$data=$this->m_master->get_st_id($id_st);
		echo json_encode($data);
	}

	public function tambah(){
		$id_rev= $this->input->post('id_rev');
        if($id_rev==0) {
        $array = array(
        		'id_terapis' => $this->input->post('id_terapis'),
                'id_su' => $this->input->post('id_su'),
                'id_st' => $this->input->post('id_st'),
                'qty_su' => $this->input->post('qty_su'),
                'jml_su' => $this->input->post('jml_su'),
                'qty_st' => $this->input->post('qty_st'),
                'jml_st' => $this->input->post('jml_st'),
                'jml_stsu' => $this->input->post('jml_stsu'),
                'pb_rev' => $this->input->post('pb_rev'),
                'tgl_rev' => $this->input->post('tgl_rev'),
                'grand_total' => $this->input->post('grand_total')
            );
            $query = $this->m_master->tambahRev($array);
        } else {
           $array = array(
        		'id_terapis' => $this->input->post('id_terapis'),
                'id_su' => $this->input->post('id_su'),
                'id_st' => $this->input->post('id_st'),
                'qty_su' => $this->input->post('qty_su'),
                'jml_su' => $this->input->post('jml_su'),
                'qty_st' => $this->input->post('qty_st'),
                'jml_st' => $this->input->post('jml_st'),
                'jml_stsu' => $this->input->post('jml_stsu'),
                'pb_rev' => $this->input->post('pb_rev'),
                'tgl_rev' => $this->input->post('tgl_rev'),
                'grand_total' => $this->input->post('grand_total')
            );
            $query = $this->m_master->editRev($id_rev,$array);
        }
        redirect('master/revenue');
    }

    public function delete($id_rev) {
    	
    	$query = $this->m_master->deleteRev($id_rev);
    	redirect('master/revenue');

    }

    
	
}
