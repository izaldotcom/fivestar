<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class grade_terapis extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
		if($this->session->userdata('status') != "login"){
		   redirect(base_url("admin/login"));
		  }
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);

		//$this->load->view('welcome_message');
		$data = '';
		
		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['grade_terapis'] = $this->m_master->getGradeTerapis()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('admin/grade_terapis', $this->data);
	}

	public function tambah(){
		$id_grade= $this->input->post('id_grade');
        if(empty($id_grade)) {
        $array = array(
                'grade_type' => $this->input->post('grade_type'),
                'deskripsi' => $this->input->post('deskripsi')
            );
            $query = $this->m_master->tambahGradeTerapis($array);
        } else {
            $array = array(
                'grade_type' => $this->input->post('grade_type'),
                'deskripsi' => $this->input->post('deskripsi')
            );
            $query = $this->m_master->editGradeTerapis($id_grade,$array);
        }
        redirect('admin/grade_terapis');
    }

     function delete($id_grade) {
    	
    	$query = $this->m_master->deleteGradeTerapis($id_grade);
    	redirect('admin/grade_terapis');

    }
	
}
