<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class servis_utama extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
		if($this->session->userdata('status') != "login"){
		   redirect(base_url("admin/login"));
		  }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);
		//$this->load->view('welcome_message');
		$data = '';
		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['servis'] = $this->m_master->getSU()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('admin/servis_utama', $this->data);
	}


	public function tambah(){
		$id_su= $this->input->post('id_su');
        if(empty($id_su)) {
        $array = array(
                'kode_su' => $this->input->post('kode_su'),
                'nama_su' => $this->input->post('nama_su'),
                'rate_su' => $this->input->post('rate_su'),
                'diskon_su' => $this->input->post('diskon_su')
            );
            $query = $this->m_master->tambahSU($array);
        } else {
            $array = array(
                'kode_su' => $this->input->post('kode_su'),
                'nama_su' => $this->input->post('nama_su'),
                'rate_su' => $this->input->post('rate_su'),
                'diskon_su' => $this->input->post('diskon_su')

            );
            $query = $this->m_master->editSU($id_su,$array);
        }
        redirect('admin/servis_utama');
    }

     function delete($id_su) {
    	
    	$query = $this->m_master->deleteSU($id_su);
    	redirect('admin/servis_utama');

    }

	
}
