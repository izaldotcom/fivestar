<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_rev_harian extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
		if($this->session->userdata('status') != "login"){
		   redirect(base_url("admin/login"));
		  }
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);

		//$this->load->view('welcome_message');
		$data = '';
		
		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['terapis'] = $this->m_master->getTerapis()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('admin/terapis', $this->data);
	}

	public function tambah(){
		$id_terapis= $this->input->post('id_terapis');
        if($id_terapis==0) {
        $array = array(
                'kode_terapis' => $this->input->post('kode_terapis'),
                'nama_terapis' => $this->input->post('nama_terapis'),
                'rate_terapis' => $this->input->post('rate_terapis'),
                'alamat' => $this->input->post('alamat'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'jk' => $this->input->post('jk'),
                'no_telp' => $this->input->post('no_telp')
            );
            $query = $this->m_master->tambahTerapis($array);
        } else {
            $array = array(
                'kode_terapis' => $this->input->post('kode_terapis'),
                'nama_terapis' => $this->input->post('nama_terapis'),
                'rate_terapis' => $this->input->post('rate_terapis'),
                'alamat' => $this->input->post('alamat'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'jk' => $this->input->post('jk'),
                'no_telp' => $this->input->post('no_telp')

            );
            $query = $this->m_master->editTerapis($id_terapis,$array);
        }
        redirect('admin/terapis');
    }

     function delete($id_terapis) {
    	
    	$query = $this->m_master->deleteTerapis($id_terapis);
    	redirect('admin/terapis');

    }
	
}
