<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dalam_negeri extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);
		//$this->load->view('welcome_message');
		$data = '';
		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('pengajuan/dalam_negeri', $this->data);
	}

	function proses_upload(){

        $config['upload_path']   = FCPATH.'/upload_dokumen/';
        $config['allowed_types'] = '*';
        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){
        	$token=$this->input->post('token_foto');
        	$nama=$this->upload->data('file_name');
        	$this->db->insert('foto',array('nama_foto'=>$nama,'token'=>$token));
        }


	}


	//Untuk menghapus foto
	function remove_foto(){

		//Ambil token foto
		$token=$this->input->post('token');

		
		$foto=$this->db->get_where('foto',array('token'=>$token));


		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_foto=$hasil->nama_foto;
			if(file_exists($file=FCPATH.'/upload_dokumen/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('foto',array('token'=>$token));

		}


		echo "{}";
	}
}
