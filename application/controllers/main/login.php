<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	private $valid = false;

	function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_login');
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);
		//$this->load->view('welcome_message');
		$data = '';
		//$this->data['header'] = $this->load->view('include/header', $this->data, true);
		//$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('main/login', $this->data);
	}

	public function cek_login(){
	  $username = $this->input->post('username');
	  $password = $this->input->post('password');
	  
	  $where = array(
	   'username' => $username,
	   'password' => md5($password)
	   );
	  $cek = $this->m_login->cek_login("m_user",$where)->num_rows();
	  if($cek > 0){

	   $data_session = array(
	    'nama' => $username,
	    'status' => "login"
	    );

	   $this->session->set_userdata($data_session);

	   redirect(base_url("main/home"));

	  }else{ 
    		$this->session->set_flashdata('warning', 'Password yang anda masukkan salah');
    	}
		echo '<script>window.location.assign("'.base_url('main/login').'");</script>';
	 }

	public function logout(){
	  $this->session->sess_destroy();
	  redirect(base_url('main/login'));
	 }
	}
