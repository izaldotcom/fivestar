<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class servis_tambahan extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
		if($this->session->userdata('status') != "login"){
		   redirect(base_url("login"));
		  }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);
		//$this->load->view('welcome_message');
		$data = '';
		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['servis'] = $this->m_master->getST()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('master/servis_tambahan', $this->data);
	}


	public function tambah(){
		$id_st= $this->input->post('id_st');
        if($id_st==0) {
        $array = array(
                'kode_st' => $this->input->post('kode_st'),
                'nama_st' => $this->input->post('nama_st'),
                'rate_st' => $this->input->post('rate_st'),
                'diskon_st' => $this->input->post('diskon_st')
            );
            $query = $this->m_master->tambahST($array);
        } else {
            $array = array(
                'kode_st' => $this->input->post('kode_st'),
                'nama_st' => $this->input->post('nama_st'),
                'rate_st' => $this->input->post('rate_st'),
                'diskon_st' => $this->input->post('diskon_st')

            );
            $query = $this->m_master->editST($id_st,$array);
        }
        redirect('master/servis_tambahan');
    }

     function delete($id_st) {
    	
    	$query = $this->m_master->deleteST($id_st);
    	redirect('master/servis_tambahan');

    }

	
}
