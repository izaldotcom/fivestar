<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customer extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->helper('text');
		$this->load->library('session');
		$this->load->model('m_master');
        if($this->session->userdata('status') != "login"){
           redirect(base_url("login"));
          }
		
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		error_reporting(0);

		//$this->load->view('welcome_message');
		$data = '';

		$this->data['header'] = $this->load->view('include/header', $this->data, true);
		$this->data['headbar'] = $this->load->view('include/headbar', $this->data, true);
		$this->data['footer'] = $this->load->view('include/footer', $this->data, true);
		$this->data['customer'] = $this->m_master->getCustomer()->result();
        //$this->load->view('theme/'.$tema.'/index', $this->data);
        $this->load->view('master/customer', $this->data);
	}

	public function tambah(){
		$id_customer= $this->input->post('id_customer');
        if($id_customer==0) {
        $array = array(
                'kode_customer' => $this->input->post('kode_customer'),
                'nama_customer' => $this->input->post('nama_customer'),
                'alamat' => $this->input->post('alamat'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'jk' => $this->input->post('jk'),
                'no_telp' => $this->input->post('no_telp'),
                'pekerjaan' => $this->input->post('pekerjaan')
            );
            $query = $this->m_master->tambahCustomer($array);
        } else {
            $array = array(
                'kode_customer' => $this->input->post('kode_customer'),
                'nama_customer' => $this->input->post('nama_customer'),
                'alamat' => $this->input->post('alamat'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'jk' => $this->input->post('jk'),
                'no_telp' => $this->input->post('no_telp'),
                'pekerjaan' => $this->input->post('pekerjaan')

            );
            $query = $this->m_master->editCustomer($id_customer,$array);
        }
        redirect('master/customer');
    }

     function delete($id_customer) {
    	
    	$query = $this->m_master->deleteCustomer($id_customer);
    	redirect('master/customer');

    }
	
}
