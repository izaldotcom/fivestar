<?php echo $header; ?>
<?php echo $headbar; ?>
<?php
$kodecustomer = $this->m_master->kodecustomer();
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Customer</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>Form validation <small>sub title</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-bottom:5px;"> <i class="fa fa-plus"></i> Tambah Customer</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>List Customer</h3>
                    <!-- <h2>Default Example <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                   <!--  <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Kode Customer</th>
                          <th>Nama Customer</th>
                          <th>Alamat</th>
                          <th>Tanggal Lahir</th>
                          <th>Jenis Kelamin</th>
                          <th>No Telp</th>
                          <th>Pekerjaan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php
                        $no=0; 
                        foreach ($customer as $data) {
                        $no++;
                        ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $data->kode_customer; ?></td>
                          <td><?= $data->nama_customer; ?></td>
                          <td><?= $data->alamat; ?></td>
                          <td><?= $data->tgl_lahir; ?></td>
                          <td><?= $data->jk; ?></td>
                          <td><?= $data->no_telp; ?></td>
                          <td><?= $data->pekerjaan; ?></td>
                          <td><div class="btn-group">
                    
                    <button class="btn btn-warning btn-flat" data-toggle="modal" data-target="#myModal"  title="Edit Data : <?=$data->kode_customer;?> " onclick="edit_user('<?= $data->id_customer; ?>','<?= $data->kode_customer; ?>','<?= $data->nama_customer; ?>','<?= $data->alamat; ?>','<?= $data->tgl_lahir; ?>','<?= $data->jk; ?>','<?= $data->no_telp; ?>','<?= $data->pekerjaan; ?>')">
                    <i class="fa fa-edit"></i>
                    </button>
                    
                    <a href="<?php echo base_url('master/customer/delete/'.$data->id_customer); ?>" title="" onclick="return confirm('Apakah anda yakin ingin menghapus <?= $data->nama_customer; ?>?');"> 
                      <button class="btn  btn-danger btn-flat"  title="Hapus">
                        <i class="fa fa-trash"></i>
                      </button>
                    </a>
                  </div>
                </td></td>
                        </tr>
                       
                      </tbody>
                      <?php
                  //end loop
                      }
                      ?>
                    </table>
                  </div>
                </div>
              </div>
          </div>

        </div>
<script>
  function edit_user(id_customer,kode_customer,nama_customer,alamat,tgl_lahir,jk,no_telp,pekerjaan){
    document.getElementById("id_customer").value=id_customer;
    document.getElementById("kode_customer").value=kode_customer;
    document.getElementById("nama_customer").value=nama_customer;
    document.getElementById("alamat").value=alamat;
    document.getElementById("tgl_lahir").value=tgl_lahir;
    document.getElementById("jk").value=jk;
    document.getElementById("no_telp").value=no_telp;
    document.getElementById("pekerjaan").value=pekerjaan; 
  }
</script>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
  <div class="modal-body">
    <form class="form-horizontal" role="form" method="post" action="<?php echo site_url('master/customer/tambah')?>" enctype="multipart/form-data">
    <input type="hidden" value="0" class="form-control" id="id_customer" name="id_customer" required>
      <div class="form-group">
        <label class="control-label col-sm-3" for="no">Kode Customer : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="kode_customer" name="kode_customer" value="<?= $kodecustomer; ?>" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Nama Customer : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="nama_customer" name="nama_customer">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Alamat : </label>
        <div class="col-sm-9">
          <textarea class="form-control" name="alamat" rows="5"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Tanggal Lahir : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Jenis Kelamin : </label>
        <div class="col-sm-9">
          <select name="jk" class="form-control">
            <option value="" disabled selected>-- Pilih Jenis Kelamin --</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">No. Telepon : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="no_telp" name="no_telp">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Pekerjaan : </label>
        <div class="col-sm-9">
          <select name="pekerjaan" class="form-control">
            <option value="" disabled selected>-- Pilih Jenis Pekerjaan --</option>
            <option value="Pegawai Swasta">Pegawai Swasta</option>
            <option value="PNS">PNS</option>
            <option value="Wirausaha">Wirausaha</option>
          </select>
        </div>
       </div>
     
      <div class="form-group">
        <div class="col-sm-7"> 
          
        </div>
        <div class="col-sm-5">
          <div class="pull-right">

            <button class="btn btn-default btn-flat" data-dismiss="modal"> <i class="fa fa-remove"></i> Close</button>


              <button type="submit" class="btn btn-primary btn-flat" name="upload"><i class="fa fa-save"></i> Simpan</button>
    </div>  
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>


        <!-- /page content -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- <script src="<?= base_url('assets');?>/vendors/jquery2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets');?>/vendors/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets');?>/vendors/basic.min.css">
<script type="text/javascript" src="<?= base_url('assets');?>/vendors/dropzone.min.js"></script>
<script type="text/javascript">
$(".delete").on('click', function() {
  $('.case:checkbox:checked').parents("tr").remove();
    $('.check_all').prop("checked", false); 
  check();

});
var i=2;
$(".addmore").on('click',function(){
  count=$('table tr').length;
    var data="<tr><td><input type='checkbox' class='case'/></td><td><span id='no"+i+"'>"+count+".</span></td>";
    data +="<td><input type='text' id='nik_peserta"+i+"' name='nik_peserta"+i+"' size='8' class='form-control'/></td> <td><input type='text' id='nama_peserta"+i+"' name='nama_peserta"+i+"' size='25' class='form-control'/></td><td><input type='text' id='grade_peserta"+i+"' name='grade_peserta"+i+"' size='5' class='form-control'/></td><td><input type='text' id='jabatan_peserta"+i+"' name='jabatan_peserta"+i+"' size='25' class='form-control'/></td><td><input type='text' id='unit_kerja"+i+"' name='unit_kerja"+i+"' size='20' class='form-control'/></td><td><input type='text' id='jumlah_peserta"+i+"' name='jumlah_peserta"+i+"' size='3' class='form-control'/></td></tr>";
  $('table').append(data);
  i++;
});

function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}

function check(){
  obj=$('table tr').find('span');
  $.each( obj, function( key, value ) {
  id=value.id;
  $('#'+id).html(key+1);
  });
  }

</script>
<script type="text/javascript">

Dropzone.autoDiscover = false;

var foto_upload= new Dropzone(".dropzone",{
url: "<?= base_url('index.php/pengajuan/dalam_negeri/proses_upload') ?>",
maxFilesize: 5,
method:"post",
acceptedFiles:".png,.jpg,.jpeg,.png,.doc,.docx,.txt,.xls,.xlsx,.pdf",
paramName:"userfile",
dictInvalidFileType:"Type file ini tidak dizinkan",
addRemoveLinks:true,
});


//Event ketika Memulai mengupload
foto_upload.on("sending",function(a,b,c){
  a.token=Math.random();
  c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
});


//Event ketika foto dihapus
foto_upload.on("removedfile",function(a){
  var token=a.token;
  $.ajax({
    type:"post",
    data:{token:token},
    url:"<?= base_url('index.php/pengajuan/dalam_negeri/remove_foto') ?>",
    cache:false,
    dataType: 'json',
    success: function(){
      console.log("Foto terhapus");
    },
    error: function(){
      console.log("Error");

    }
  });
});


</script>   -->  
<?php echo $footer; ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( "#tgl_lahir" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2018"

    });
</script>