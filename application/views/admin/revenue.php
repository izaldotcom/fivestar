<?php echo $header; ?>
<?php echo $headbar; ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Revenue</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>Form validation <small>sub title</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-bottom:5px;"> <i class="fa fa-plus"></i> Tambah Data</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>List Revenue</h3>
                    <!-- <h2>Default Example <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                   <!--  <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p> -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr">
                          <th rowspan="2" align="center">No</th>
                          <th rowspan="2" align="center">Tanggal</th>
                          <th colspan="2"><center>Terapis</center></th>
                          
                          <th colspan="6"><center>Servis Utama</center></th>
                          
                          <th colspan="6"><center>Servis Tambahan</center></th>
                          
                          <th colspan="2"><center>Utama & Tambahan</center></th>
                          <th rowspan="2" align="center">Grand Total</th>
                          <th rowspan="2"><center>Aksi</center></th> 
                        </tr>
                      
                        <tr>
                          <th>Kode</th>
                          <th>Nama</th>
                         
                          <th>Kode</th>
                          <th>Nama Servis</th>
                          <th>Qty</th>
                          <th>Rate</th>
                          <th>Diskon</th>
                          <th>Jumlah</th>
                          
                          <th>Kode</th>
                          <th>Nama Servis</th>
                          <th>Qty</th>
                          <th>Rate</th>
                          <th>Diskon</th>
                          <th>Jumlah</th>
                          
                          <th>Total</th>
                          <th>PB-1</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                        <?php
                        $no=0; 
                        foreach ($revenue as $data) {
                        $no++;
                        ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $data->tgl_rev; ?></td>
                          <td><?= $data->kode_terapis; ?></td>
                          <td><?= $data->nama_terapis; ?></td>
                          
                          <td><?= $data->kode_su; ?></th>
                          <td><?= $data->nama_su; ?></td>
                          <td><?= $data->qty_su; ?></td>
                          <td><?= $data->rate_su; ?></td>
                          <td><?= $data->diskon_su; ?>%</td>
                          <td><?= $data->jml_su; ?></td>
                          
                          <td><?= $data->kode_st; ?></td>
                          <td><?= $data->nama_st; ?></td>
                          <td><?= $data->qty_st; ?></td>
                          <td><?= $data->rate_st; ?></td>
                          <td><?= $data->diskon_st; ?>%</td>
                          <td><?= $data->jml_st; ?></td>
                          
                          <td><?= $data->jml_stsu; ?></td>
                          <td><?= $data->pb_rev; ?></td>
                          <td><?= $data->grand_total; ?></td>
                          <td><div class="btn-group">
                          <button class="btn btn-warning btn-flat" data-toggle="modal" data-target="#myModal"  title="Edit Data : <?=$data->tgl_rev;?> " onclick="edit_user('<?= $data->id_rev; ?>','<?= $data->tgl_rev; ?>','<?= $data->id_terapis; ?>','<?= $data->id_su; ?>','<?= $data->qty_su; ?>','<?= $data->id_st; ?>','<?= $data->qty_st; ?>','<?= $data->pb_rev; ?>')">
                          <i class="fa fa-edit"></i>
                          </button>
                          <a href="<?php echo base_url('master/revenue/delete/'.$data->id_rev); ?>" title="" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?');"> 
                          <button class="btn  btn-danger btn-flat"  title="Hapus">
                          <i class="fa fa-trash"></i></button></a></div>
                          </td>
                        </tbody>
                      <?php
                  //end loop
                      }
                      ?>
                    </table>
                  </div>
                </div>
              </div>
          </div>

        </div>
<script>
  function edit_user(id_rev,id_terapis,id_su,id_st,qty_su,qty_st,pb_rev){
    document.getElementById("id_rev").value=id_rev;
    document.getElementById("id_terapis").value=id_terapis;
    document.getElementById("id_su").value=id_su;
    document.getElementById("id_st").value=id_st;
    document.getElementById("qty_su").value=qty_su;
    document.getElementById("qty_st").value=qty_st;
    document.getElementById("pb_rev").value=pb_rev; 
  }
</script>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
  <div class="modal-body">
    <form class="form-horizontal" role="form" method="post" action="<?php echo site_url('master/revenue/tambah')?>" enctype="multipart/form-data">
    <input type="hidden" value="0" class="form-control" id="id_rev" name="id_rev" required>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Tanggal Revenue : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="tgl_rev" name="tgl_rev" required>
        </div>
      </div><br>
      <label for="name" style="color: red">Terapis</label>
      <div class="form-group">
        <label class="control-label col-sm-3" for="no">Kode Terapis : </label>
        <div class="col-sm-9">
          <select class="form-control" name="id_terapis" id="id_terapis">
                            <option value="">Pilih</option>
                            <?php
                            foreach ($terapis as $terapis) {
                                ?>
                                <option <?php echo $terapis_selected == $terapis->id_terapis ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $terapis->id_terapis ?>"><?php echo $terapis->kode_terapis ?></option>
                                <?php
                            }
                            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Nama Terapis : </label>
        <div class="col-sm-9">
          
          <input type="text" class="form-control" id="nama_terapis" name="nama_terapis" readonly="">
         
        </div>
      </div><br>
      <label for="name" style="color: red">Treatment Utama</label>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Kode Servis : </label>
        <div class="col-sm-9">
          <select class="form-control" name="id_su" id="id_su">
                            <option value="">Pilih</option>
                            <?php
                            foreach ($su as $su) {
                                ?>
                                <option <?php echo $su_selected == $su->id_su ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $su->id_su ?>"><?php echo $su->kode_su ?></option>
                                <?php
                            }
                            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Nama Servis : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="nama_su" name="nama_su" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Qty : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="qty_su" name="qty_su" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Rate : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="rate_su" name="rate_su" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Diskon : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="diskon_su" name="diskon_su" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Total Rate : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="jml_su" name="jml_su" readonly>
        </div>
      </div><br>
      <label for="name" style="color: red">Treatment Tambahan</label>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Kode Servis : </label>
        <div class="col-sm-9">
          <select class="form-control" name="id_st" id="id_st">
                            <option value="">Pilih</option>
                            <?php
                            foreach ($st as $st) {
                                ?>
                                <option <?php echo $st_selected == $st->id_st ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $st->id_st ?>"><?php echo $st->kode_st ?></option>
                                <?php
                            }
                            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Nama Servis : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="nama_st" name="nama_st" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Qty : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="qty_st" name="qty_st" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Rate : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="rate_st" name="rate_st" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Diskon : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="diskon_st" name="diskon_st" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Total Rate : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="jml_st" name="jml_st" readonly>
        </div>
      </div><br>
      <label for="name" style="color: red">Total Semua Treatment</label>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Jumlah Utama & Tambahan : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="jml_stsu" name="jml_stsu" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">PB-1 : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="pb_rev" name="pb_rev" readonly>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="name">Grand Total : </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="grand_total" name="grand_total" readonly>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-7"> 
          
        </div>
        <div class="col-sm-5">
          <div class="pull-right">

            <button class="btn btn-default btn-flat" data-dismiss="modal"> <i class="fa fa-remove"></i> Tutup</button>


              <button type="submit" class="btn btn-primary btn-flat" name="upload"><i class="fa fa-save"></i> Simpan</button>
    </div>  
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>


        <!-- /page content -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- <script src="<?= base_url('assets');?>/vendors/jquery2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets');?>/vendors/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets');?>/vendors/basic.min.css">
<script type="text/javascript" src="<?= base_url('assets');?>/vendors/dropzone.min.js"></script>
<script type="text/javascript">
$(".delete").on('click', function() {
  $('.case:checkbox:checked').parents("tr").remove();
    $('.check_all').prop("checked", false); 
  check();

});
var i=2;
$(".addmore").on('click',function(){
  count=$('table tr').length;
    var data="<tr><td><input type='checkbox' class='case'/></td><td><span id='no"+i+"'>"+count+".</span></td>";
    data +="<td><input type='text' id='nik_peserta"+i+"' name='nik_peserta"+i+"' size='8' class='form-control'/></td> <td><input type='text' id='nama_peserta"+i+"' name='nama_peserta"+i+"' size='25' class='form-control'/></td><td><input type='text' id='grade_peserta"+i+"' name='grade_peserta"+i+"' size='5' class='form-control'/></td><td><input type='text' id='jabatan_peserta"+i+"' name='jabatan_peserta"+i+"' size='25' class='form-control'/></td><td><input type='text' id='unit_kerja"+i+"' name='unit_kerja"+i+"' size='20' class='form-control'/></td><td><input type='text' id='jumlah_peserta"+i+"' name='jumlah_peserta"+i+"' size='3' class='form-control'/></td></tr>";
  $('table').append(data);
  i++;
});

function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}

function check(){
  obj=$('table tr').find('span');
  $.each( obj, function( key, value ) {
  id=value.id;
  $('#'+id).html(key+1);
  });
  }

</script>
<script type="text/javascript">

Dropzone.autoDiscover = false;

var foto_upload= new Dropzone(".dropzone",{
url: "<?= base_url('index.php/pengajuan/dalam_negeri/proses_upload') ?>",
maxFilesize: 5,
method:"post",
acceptedFiles:".png,.jpg,.jpeg,.png,.doc,.docx,.txt,.xls,.xlsx,.pdf",
paramName:"userfile",
dictInvalidFileType:"Type file ini tidak dizinkan",
addRemoveLinks:true,
});


//Event ketika Memulai mengupload
foto_upload.on("sending",function(a,b,c){
  a.token=Math.random();
  c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
});


//Event ketika foto dihapus
foto_upload.on("removedfile",function(a){
  var token=a.token;
  $.ajax({
    type:"post",
    data:{token:token},
    url:"<?= base_url('index.php/pengajuan/dalam_negeri/remove_foto') ?>",
    cache:false,
    dataType: 'json',
    success: function(){
      console.log("Foto terhapus");
    },
    error: function(){
      console.log("Error");

    }
  });
});


</script>   -->  

<?php echo $footer; ?>
<script>
    $(document).ready(function(){
       $('#id_terapis').on('input',function(){
                
                var id_terapis=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('admin/revenue/get_terapis')?>",
                    dataType : "JSON",
                    data : {id_terapis: id_terapis},
                    cache:false,
                    success: function(data){
                        $.each(data,function(id_terapis, kode_terapis, nama_terapis, rate_terapis){
                            $('[name="kode_terapis"]').val(data.kode_terapis);
                            $('[name="nama_terapis"]').val(data.nama_terapis);
                            $('[name="rate_terapis"]').val(data.rate_terapis);
                            
                        });
                        
                    }
                });
                return false;
           });

    });

    $(document).ready(function(){
       $('#id_su').on('input',function(){
                
                var id_su=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('admin/revenue/get_su')?>",
                    dataType : "JSON",
                    data : {id_su: id_su},
                    cache:false,
                    success: function(data){
                        $.each(data,function(id_su, kode_su, nama_su, rate_su, diskon_su){
                            $('[name="kode_su"]').val(data.kode_su);
                            $('[name="nama_su"]').val(data.nama_su);
                            $('[name="rate_su"]').val(data.rate_su);
                            $('[name="diskon_su"]').val(data.diskon_su);
                        });
                        
                    }
                });
                return false;
           });

    });

    $(document).ready(function(){
       $('#id_st').on('input',function(){
                
                var id_st=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('admin/revenue/get_st')?>",
                    dataType : "JSON",
                    data : {id_st: id_st},
                    cache:false,
                    success: function(data){
                        $.each(data,function(id_st, kode_st, nama_st, rate_st, diskon_st){
                            $('[name="kode_st"]').val(data.kode_st);
                            $('[name="nama_st"]').val(data.nama_st);
                            $('[name="rate_st"]').val(data.rate_st);
                            $('[name="diskon_st"]').val(data.diskon_st);
                        });
                        
                    }
                });
                return false;
           });

    });


function total_su(){
  var a = $("#qty_su").val();
  var b = $("#rate_su").val();
  var c = $("#diskon_su").val();
  var hasil = (a*b)-(b*(c/100));
  $("#jml_su").val(hasil);
}
$("#qty_su,#rate_su,#diskon_su").keyup(function(){
total_su();
});

function total_st(){
  var a = $("#qty_st").val();
  var b = $("#rate_st").val();
  var c = $("#diskon_st").val();
  var hasil = (a*b)-(b*(c/100));
  $("#jml_st").val(hasil);
}
$("#qty_st,#rate_st,#diskon_st").keyup(function(){
total_st();
});

function total_stsu(){
  var a = $("#jml_su").val();
  var b = $("#jml_st").val();
  var hasil = (a*1)+(b*1);
  $("#jml_stsu").val(hasil);
}
$("#qty_st,#jml_su,#jml_st").keyup(function(){
total_stsu();
});

function pb_rev(){
  var a = $("#jml_stsu").val();
  var hasil = a*(10/100);
  $("#pb_rev").val(hasil);
}
$("#qty_st,#jml_stsu").keyup(function(){
pb_rev();
});

function grand_total(){
  var a = $("#jml_stsu").val();
  var b = $("#pb_rev").val();
  var hasil = (a/1)+(b/1);
  $("#grand_total").val(hasil);
}
$("#qty_st,#jml_stsu,#pb_rev").keyup(function(){
grand_total();
});
    
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( "#tgl_rev" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2018"

    });
</script>