<?php echo $header; ?>
<?php echo $headbar; ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Analisa Dalam Negeri</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                    <!-- Tabs -->
                    <div id="wizard_verticle" class="form_wizard wizard_verticle">
                      <ul class="list-unstyled wizard_steps">
                        <li>
                          <a href="#step-11">
                            <span class="step_no">1</span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-22">
                            <span class="step_no">2</span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-33">
                            <span class="step_no">3</span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-44">
                            <span class="step_no">4</span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-55">
                            <span class="step_no">5</span>
                          </a>
                        </li>
                        
                      </ul>

                      <div id="step-11">
                        <h2 class="StepTitle">Step 1</h2>
                        <form class="form-horizontal form-label-left">

                          <span class="section">Kriteria Peserta</span>

                          <div class="form-group" >
                            <label class="control-label" for="first-name">Diusulkan oleh pejabat yang berwenang sesuai matriks kewenangan *) <span class="required"></span>
                            </label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta1" id="kriteria_peserta1" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta1" id="kriteria_peserta1" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label class="control-label" for="last-name">Tidak sedang menjalani proses terkait dengan indikasi pelanggaran ketentuan bank maupun esternal bank <span class="required"></span>
                            </label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta2" id="kriteria_peserta2" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta2" id="kriteria_peserta2" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label for="middle-name">Tidak sedang menjalani masa hukuman baik berupa sanksi tingkat ringan sampai dengan tingkat berat dan / atau yang dipersamakan dengan itu</label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta3" id="kriteria_peserta3" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta3" id="kriteria_peserta3" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label for="middle-name" class="control-label">Belum pernah mendapatkan pelatihan sejenis</label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta4" id="kriteria_peserta4" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_peserta4" id="kriteria_peserta4" value="Tidak" />
                            </div>
                          </div>

                        </form>
                      </div>
                      <div id="step-22">
                        <h2 class="StepTitle">Step 2</h2>
                        <span class="section">Kriteria Materi</span>
                        <div class="form-group" >
                            <label class="control-label" for="first-name">Relevan dengan kebutuhan unit kerja <span class="required"></span>
                            </label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi1" id="kriteria_materi1" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi1" id="kriteria_materi1" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label class="control-label" for="last-name">Relevan dengan job position / job desk peserta <span class="required"></span>
                            </label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi2" id="kriteria_materi2" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi2" id="kriteria_materi2" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label for="middle-name" class="control-label">Relevan dengan grading peserta</label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi3" id="kriteria_materi3" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi3" id="kriteria_materi3" value="Tidak" />
                            </div>
                          </div>
                          <div class="form-group" style="padding-top: 10px;">
                            <label for="middle-name" class="control-label">Bukan merupakan materi yang disampaikan dalam pelatihan in-house training</label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi4" id="kriteria_materi4" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi4" id="kriteria_materi4" value="Tidak" />
                            </div>
                          </div>
                      </div>
                      <div id="step-33">
                        <h2 class="StepTitle">Step 3</h2>
                        <span class="section">Penyelenggara</span>
                        <div class="form-group" style="padding-top: 10px;">
                            <label for="middle-name" class="control-label">Bukan merupakan materi yang disampaikan dalam pelatihan in-house training</label>
                            <div style="padding-top: 5px;">
                              Ya
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi4" id="kriteria_materi4" value="Ya" checked="" required /> Tidak
                              <input type="radio" class="flat form-control col-md-7 col-xs-12" name="kriteria_materi4" id="kriteria_materi4" value="Tidak" />
                            </div>
                          </div>
                      <div id="step-55">
                        <h2 class="StepTitle">Step 5</h2>
                        <span class="section">Rekomendasi</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                          in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                          in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                      </div>
                    </div>
                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php echo $footer; ?>