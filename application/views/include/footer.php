 <!-- footer content -->
        <footer>
          <div class="pull-right">
            PT. PANCA ABADI - Powered by <a href="http://www.gajianlagi.com">PT. Sekar Putih Mulia Mandiri</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets');?>/vendors/jquery/dist/jquery.js"></script>
    <script src="<?= base_url('assets');?>/vendors/jquery/dist/jquery.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="<?= base_url('assets');?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets');?>/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets');?>/vendors/nprogress/nprogress.js"></script>
 
    <!-- Chart.js -->
    <script src="<?= base_url('assets');?>/vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- jQuery Smart Wizard -->
    <script src="<?= base_url('assets');?>/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- gauge.js -->
    <script src="<?= base_url('assets');?>/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= base_url('assets');?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url('assets');?>/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?= base_url('assets');?>/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= base_url('assets');?>/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= base_url('assets');?>/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url('assets');?>/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url('assets');?>/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url('assets');?>/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= base_url('assets');?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url('assets');?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= base_url('assets');?>/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?= base_url('assets');?>/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= base_url('assets');?>/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= base_url('assets');?>/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url('assets');?>/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="<?= base_url('assets');?>/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    
    <!-- Datatables -->
    <script src="<?= base_url('assets');?>/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url('assets');?>/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= base_url('assets');?>/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= base_url('assets');?>/build/js/custom.min.js"></script>
	
  </body>
</html>
<style>
.confirm-div-mob{
    position: fixed;
    background-color: rgba(31, 31, 31, 0.74);
    color: #fff;
    border: solid 1px #rgba(31, 31, 31, 0.74);
    width: 88%;
    height: 35px;
    vertical-align: middle;
    text-align: center;
    padding: 8px;
    left: 6%;
    bottom: 10px;
    display: none;
    border-radius: 20px;
}
.confirm-div-mob span{
color: #fff !important; 
}
</style>
<div class="confirm-div-mob" style="z-index:999999999999999999999999999999999999"></div>
<script>
// assumes you're using jQuery
$(document).ready(function() {
  <?php if($this->session->flashdata('message')){ ?>
  $('.confirm-div-mob').delay(200).fadeIn('normal', function() {
    $('.confirm-div-mob').html('<?php echo $this->session->flashdata('message'); ?>').show();
        $(this).delay(5800).fadeOut();
   });
    <?php } ?>
});
function resetForm() {
    document.getElementById("form").reset();
}
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( "#tgl_lahir,#tgl_join" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
      yearRange: "1930:2018"

    });
</script>