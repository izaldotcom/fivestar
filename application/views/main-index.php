<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Menu System </title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets');?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets');?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url('assets');?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url('assets');?>/vendors/animate.css/animate.min.css" rel="stylesheet">

    <link href="<?= base_url('assets');?>/build/css/custom.min.css" rel="stylesheet">
  </head>
<style type="text/css">
  .center {
    margin: auto;
    width: 55%;
    padding-right: 5px;
    padding-left: 5px;
  }
</style>
  <body class="login" style="padding-top: 5%;">
      <center>
        <div>
            <h1>PILIH MENU</h1>
        </div>
      </center>  
      <br>
         <div class="center"> 
          <a href="<?= base_url('admin/login');?>">
            <section class="col-md-5 col-sm-5 col-xs-12" style="border-style: solid;">
                <center>
                  <h1>Admin</h1>
                  <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <br />
                  <div class="img">
                    <img src="<?= base_url('assets');?>/main-index/admin.png" alt="">
                  </div>
                  <div class="clearfix"></div>  
                </center>    
            </section>
          </a>
          <section class="col-md-2 col-sm-2 col-xs-12">   
          </section>
          <a href="<?= base_url('admin/login');?>">
            <section class="col-md-5 col-sm-5 col-xs-12" style="border-style: solid;">
                <center>
                  <h1>Kasir</h1>
                  <div class="clearfix"></div>

                    <div class="clearfix"></div>
                    <br />
                  <div class="img">
                    <img src="<?= base_url('assets');?>/main-index/kasir.png" alt="">
                  </div>
                  <div class="clearfix"></div>   
                </center>     
            </section>
          </a>
        </div>
        
  </body>
</html>
